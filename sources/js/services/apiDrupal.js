import { async } from "regenerator-runtime";

class apiDrupal {
  index(){
    return 'welcome to service';
  }

  registerUser(data) {
    return new Promise( async (resolve,reject) => {
      const request = await post(`/api/user`, data);
      resolve(request);
    })
  }


  getQr(data) {
    return new Promise( async (resolve,reject) => {
      const request = await post(`/api/qr`, data);
      resolve(request);
    })
  }

  sendCode(data){
    return new Promise( async (resolve,reject) => {
      const request = await post(`/api/send-code`, data);
      resolve(request);
    })
  }

  sendJustCode(data){
    return new Promise( async (resolve,reject) => {
      const request = await post(`/api/send-just-code`, data);
      resolve(request);
    })
  }

  validateCode(data){
    return new Promise( async (resolve,reject) => {
      const request = await post(`/api/validate-code`, data);
      resolve(request);
    })
  }

  getAllCountries(){
    return new Promise (async (resolve,reject) =>{
      const request = await get(`https://api.150porciento.com/api/countries`);
      const result = request.reduce((acc,current) => {
        if(current.id == 47) acc = [...acc,current]
        return acc
      },[])
      resolve(result)
    })
  }

  consultPhone(data){
    return new Promise (async (resolve, reject) => {
      const request = await post(`/api/checker-phone`, data)
      resolve(request)
    })
  }

  getAllCities(){
    return new Promise (async (resolve, reject) => {
      const request = await get(`https://api.150porciento.com/api/country/47/cities`)
      resolve(request)
    })
  }

  getAllStates(){
    return new Promise (async (resolve, reject) =>{
      const request = await get(`https://api.150porciento.com/api/country/47/states`);
      resolve(request)
    })
  }

  getPubCities(){
    return new Promise (async (resolve, reject) => {
      const request = await get(`/api/taxonomy/bbc_cities`)
      resolve(request)
    })
  }

  getPubs(pubCity){
    return new Promise (async (resolve, reject) => {
      const request = await get(`/api/taxonomy/bbc_pubs/city_id/${pubCity}`)
      resolve(request)
    })
  }

  saveRequest(data){
    return new Promise( async (resolve,reject) => {
      const request = await post(`/api/user-update`, data);
      resolve(request);
    })
  }

  getUser(data){
    return new Promise( async (resolve,reject) => {
      const request = await post(`/api/user-data`, data);
      resolve(request);
    })
  }

}

export default apiDrupal;

function get(url) {
  return new Promise((resolve, reject) => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => resolve(data));
  });
}

function post(url, data) {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: "POST", // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .catch((error) => reject(error))
      .then((response) => resolve(response));
  });
}
