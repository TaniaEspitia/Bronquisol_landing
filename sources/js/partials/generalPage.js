import { win, doc, log, al, addEv, que, queAll, gId } from "../general/var.js";
import apiDrupal from "../services/apiDrupal.js";
import dama from "../services/dama.js";

const dm = new dama();


const api = new apiDrupal();
export const page = async () => {
    /* ON LOAD */
    const init = () => {
        const image = `<img class="img-thumbnail" src="https://www.codigos-qr.com/qr/php/qr_img.php?d=${location.href}&s=6&e=l" />`
        dm.print('qrad', image);
        win.onload = () => doc.que('.loader') && doc.que('.loader').classList.add('hide')
        setTimeout(() => { doc.que('.loader').classList.add('hide') }, 5000);
        
    }
    init()
    menu()
}
function menu(){
    let menu = doc.que('.exitoHeader__menu__btn');
    let container = doc.que('.exitoContainer--yellow');
    let logo = doc.que('.exitoHeader__logo');
    let social = doc.que('.exitoHeader__mediaSocial');
    let menuModal = doc.que('.exitoMenu');
    activeMenu(menu,[container,logo,social,menuModal])
}
function activeMenu(menu,elements){
    menu.addEventListener('click', () =>{
        elements.forEach(element => {
            element.classList.toggle("exitoActive")
        });
    })
}
export default page;
