export const routes = [
    {
        name: 'Home',
        path: '/',
        component: 'home'
    },
    {
        name: 'detalle',
        path: '/detalle',
        component: 'detail'
    },
    {
        name: 'register',
        path: '/registro',
        component: 'register'
    },
    {
        name: 'agegate',
        path: '/agegate',
        component: 'agegate'
    },
    {
        name: 'confirmation',
        path: '/confirmacion',
        component: 'confirmation'
    },
    {
        name: 'game',
        path: '/juego',
        component: 'game'
    },
    {
        name: 'request',
        path: '/resultado',
        component: 'request'
    },
    {
        name: 'checked',
        path: '/verificador',
        component: 'checked'
    },
]
