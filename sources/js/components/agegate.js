import dama from "../services/dama.js";

const dm = new dama();
const urlNot = 'https://www.tapintoyourbeer.com/agegate?destination=index.cfm'

export const page = async () => {

  const init = () => {
    verifiedItem();
    dm.click('yes', yes)
    dm.click('no', no)
  }

  init()
}

function verifiedItem() {
  const agegateVerified = localStorage.getItem('agegate-verified')
  if (agegateVerified == 'true') {
    yes()
  } else if (agegateVerified == 'false') {
    // dm.hide(['buttons'])
  }
}

function yes() {
  const agegateVerified = localStorage.setItem('agegate-verified', true);
  const urlRedirect = dm.getParamUrl('redirect');
  location = urlRedirect ? urlRedirect : '/'
}

function no() {
  localStorage.setItem('agegate-verified', false);
  location = urlNot;
}
