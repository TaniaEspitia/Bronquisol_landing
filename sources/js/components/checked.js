import apiDrupal from "../services/apiDrupal.js"
import dama from "../services/dama.js";

import {
    win,
    doc,
    log,
    al,
    addEv,
    que,
    queAll,
    gId
} from "../general/var.js"

const dm = new dama();
const api = new apiDrupal();
let selectCities, selectPubs;

export const page = async () => {
    const init = () => {
            initPage()
            dm.click('submit', submit)
    }
    init()
}

// Init page
async function initPage(){
    const cities =  await api.getPubCities()
    selectCities = createChoice('citiesSelect')
    selectPubs = createChoice('pubSelect')
    
    selectCities = selectData(selectCities, cities['data'], 'citiesSelect', 'Ciudades')

    changeCity();
}

function createChoice(id){
    const selectItem = document.getElementById(id);
    return new Choices(selectItem, {
        searchResultLimit: 2,
        classNames: {
            containerOuter: `choices ${id}`,
        }
    })
}

function selectData(choice, data, id, textAlternative){
    choice.destroy()
    setDataSelect(data, id, textAlternative)
    return createChoice(id)
}


function changeCity(){
    const element = document.getElementById('citiesSelect');
    element.addEventListener('change', async (event) => {
        const valueSelected = event.target.value;
        const result = await api.getPubs(valueSelected)
        selectPubs = selectData(selectPubs, result['data'], 'pubSelect', 'Seleccione el Pub')
    });
}


function setDataSelect(data, id, textAlternative) {
    const dropdownData = document.getElementById(id);
    dropdownData.innerHTML = `<option value="" hidden selected>${textAlternative}</option>`;
    const defaultOption = dropdownData.innerHTML
    const listItems = data.reduce((acc, current) => {
      if (current) {
        acc += `<option value="${current['id']}">${current['name']}</option>`;
        return acc;
      }
    }, '');
    if (dropdownData) dropdownData.innerHTML = `${defaultOption}${listItems}`;
    return dropdownData
}


function submit(){
    let selectedCity = document.getElementById('citiesSelect');
    let selectedCityText = selectedCity.options[selectedCity.selectedIndex].text

    let selectedPub = document.getElementById('pubSelect');
    let selectedPubText = selectedPub.options[selectedPub.selectedIndex].text

    if(selectedCity.value && selectedPub.value){
        location.href = `${location.href}&city=${selectedCityText}&pub=${selectedPubText}`
    }else{
        console.log('Im hereeeee')
    }

}