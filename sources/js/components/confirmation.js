// Import variables
import {
    win,
    doc,
    log,
    al,
    addEv,
    que,
    queAll,
    gId
  } from "../general/var.js"
// Import API
import apiDrupal from "../services/apiDrupal.js";
import dama from "../services/dama.js";

const api = new apiDrupal();
const dm = new dama();
const phone = localStorage.getItem('up')
const code = Math.floor(Math.random() * 900) + 100
// if(!phone) location = '/'

export const page = async () => {

    const init = () => {
        initPage()
        sendCode()
        dm.click('submit', validateCode)

    }
    init()

}

// When the page load
function initPage() {

    // FORM CODE
    const formCode = doc.que('.form--code')

    if (formCode) {


      const inputsCode = formCode.queAll('.form__input')

      Array.from(inputsCode).map(inputEl => {
        const formBox = inputEl.parentElement,
          nextFormBox = formBox.nextElementSibling,
          maxlength = parseInt(inputEl.getAttribute('maxlength')),
          maxChars = 1,
          aloneNumbers = [96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57]

        // Event listening for a change in input
        inputEl.addEv('keydown', e => {
          if (e.keyCode === 69) e.preventDefault()

          if (aloneNumbers.indexOf(e.keyCode) >= 0) {
            setTimeout(() => {
              if (inputEl.value.length > maxlength) {
                const newValue = inputEl.value.substring(1)
                inputEl.value = newValue
              }
              if (nextFormBox !== null) {
                if (inputEl.value.length == maxlength) {
                  nextFormBox.que('.form__input').focus()
                }
              }
            }, 0)
          }
        })
        
        
      })
      
    }
}


async function sendCode(){
    dm.print('request', 'Enviando el código');
    const request = await api.sendJustCode({phone, code})

    if(request.userFound) location = '/'
    else dm.print('request', request.message);
}


async function validateCode(){
    const ids = ['code1', 'code2', 'code3'],
      inputs = dm.getInpunts(ids),
      getCode = `${inputs['code1']}${inputs['code2']}${inputs['code3']}`

    const errors = dm.errorsCode(ids)
    if( errors ) return false

    const isCode = code == getCode ? true : false;

    if(isCode){
      localStorage.setItem('pv', true)
      location = '/registro' 
    } 
    else dm.print('request', 'El código ingresado no es correcto');

}