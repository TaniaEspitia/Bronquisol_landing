import apiDrupal from "../services/apiDrupal.js";
import dama from "../services/dama.js";

const api = new apiDrupal();
const dm = new dama();

const token = localStorage.getItem('ut');
// if(!token) location = '/'

export const page = async () => {
  const init = () => {
      getUser()
  }
  init()
}

async function getUser(){
    let user = await api.getUser({token});
    const requestGame = user['request'] == "1" ? true : false
    if(requestGame){
        getQr()
    }else{
        dm.print('qr', 'Perdiste');
    }
    console.log(requestGame);
}


async function getQr(){

    const request = await api.getQr({token})
    dm.print('qr', request.qr);
}