import apiDrupal from "../services/apiDrupal.js";
import dama from "../services/dama.js";

const api = new apiDrupal();
const dm = new dama();

const token = localStorage.getItem('ut');
// if(!token) location = '/'

export const page = async () => {
  const init = () => {
    getQr()
  }
  init()
}


async function getQr(){

    const request = await api.getQr({token})
    dm.print('qr', request.qr);
}
