import { win, doc, log, al, addEv, que, queAll, gId } from "../general/var.js"
/* IMPORT PARTIALS */
import { modal } from '../partials/modal.js'
import 'regenerator-runtime/runtime'

import apiDrupal from "../services/apiDrupal.js";
import dama from "../services/dama.js";
import Splide from '@splidejs/splide';

const api = new apiDrupal();
const dm = new dama();

export const page = async () => {
  const init = () => {
    new Splide( '.splide--games', {
      type   : 'loop',
      perPage: 1,
      autoWidth: true,
      gap: '5px',
	    focus  : 'center',
    } ).mount();
    try {
      initPage()
      dm.termsGetValue()
      dm.click('formPhone', sendPhone)
    } catch (error) {
      console.log(error)
    }
  }
  
  init()
}

// Send phone
async function sendPhone(e) {

  e.preventDefault()
  // Get data
  const validationPhone = doc.que('#validationPhone'),
    phoneValue = validationPhone.value

  let finalValue

  // Validate
  const validation = validatePhone(validationPhone, phoneValue)

  // Send request
  if (validation === false) return false

  finalValue = phoneValue
  const response = await sendPhoneAPI(phoneValue)
  dataDataLayers('next')
  // Target response
  // user exist, no verified 
  // console.log('Felipe', response);
  if (response.code === 1 || response.code === 3){
    localStorage.setItem('up', phoneValue)
    location.href = '/confirmacion'
  } 
  // user exist and verified
  else if (response.code === 2){
    localStorage.setItem('ut', response.token)
    location.href = '/juego'
  } 
}

// Phone validate
function validatePhone(inputEl, dataValue) {

  const regexNumbers = /^[\d]+/,
    labelInput = inputEl.parentElement.que('.form__message')

  // Validate according by regex and length
  if (regexNumbers.test(dataValue) === false || dataValue.length !== 10) {
    inputEl.classList.add('error')
    labelInput.textContent = 'Ingrese un número de celular válido'
    return false
  } else {
    inputEl.classList.remove('error')
    labelInput.textContent = ''
    return true
  }

}

// Send phone to API
async function sendPhoneAPI(data) {
  const dataToSend = {
    'phone': data
  }
  const request = await api.consultPhone(dataToSend)
  return request
}


function initPage() {

  doc.addEv('DOMContentLoaded', e => {

    // BIRTHDAY
    const birthday = doc.que('#birthday')

    if (birthday) {
      birthday.addEv('keydown', resetDate)
    }

    // RESET DATE
    function resetDate(e) {

      const aloneNumbers = [8, 46, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57],
        elDate = e.target,
        dateLength = elDate.value.length

      // Validating the key code 
      if (aloneNumbers.indexOf(e.keyCode) === -1) {
        e.preventDefault()
      }

      // Adding the slash
      if (e.keyCode !== 8 && e.keyCode !== 46) {
        if (dateLength !== 1 || dateLength !== 3) {
          if (e.keyCode == 47) {
            e.preventDefault()
          }
        }

        if (dateLength === 2 || dateLength === 5) {
          e.target.value += '/'
        }
      }

    }


    // SCROLL SCROLL
    const scrollDown = doc.que('.scrollDown')

    if (scrollDown) {
      scrollDown.addEv('click', e => {
        doc.que('.refScroll').scrollIntoView({
          block: 'center',
          behavior: 'smooth'
        })
      })
    }

    // MODAL
    modal()

    // CHOICES
    const selects = doc.queAll('.form__select')

    if (selects.length > 0) {
      Array.from(selects).map(async selectItem => {
        // let temp
        // if(selectItem.id == "country"){
        //   const countries = await getAllCountries()
        //   setConuntriesSelect(countries)
        // }

        if (selectItem.id.toLowerCase() === 'city') {

          const cities = await getAllCities()
          setStatesSelect(cities)
          const selectEl = new Choices(selectItem, {
            searchResultLimit: 2,
            classNames: {
              containerOuter: 'choices city',
            }
          })

        } else {

          const selectEl = new Choices(selectItem, {
            searchResultLimit: 2
          })

        }

      })
    }


  })
}

function dataDataLayers(select, data = []){

  let dataDataLayers = [
      {
          'name': 'next',
          'event': 'trackEvent',
          'eventCategory': 'Registro BBC', // Categoría del evento (String). Requerido.
          'eventAction': 'Teléfono', // Acción o subcategoría del evento (String). Requerido.
          'eventLabel': '' // Etiqueta de descripción del evento (String). Requerido.
      },
      {
          'name': 'register',
          'event': 'trackEvent',
          'eventCategory': 'Registro BBC', // Categoría del evento (String). Requerido.
          'eventAction': 'Registro', // Acción o subcategoría del evento (String). Requerido.
          'eventLabel': CryptoJS.SHA256(data['email']) // Etiqueta de descripción del evento (String). Requerido.
      }    
    ]
    const dataLayerFound =  dataDataLayers.find( value => value.name == select);
}