import {
  win,
  doc,
  log,
  al,
  addEv,
  que,
  queAll,
  gId
} from "../general/var.js"
/* IMPORT PARTIALS */
import {
  modal
} from '../partials/modal.js'
import 'regenerator-runtime/runtime'

import apiDrupal from "../services/apiDrupal.js";
import dama from "../services/dama.js";

const phone = localStorage.getItem('up')
const phoneVerified = localStorage.getItem('pv')
// if(!phone || !phoneVerified) location = '/'

const api = new apiDrupal();
const dm = new dama();
const inputsForm = [
  {
    id: 1,
    name: "name",
    type: "text",
    required: true
  },
  {
    id: 2,
    name: "birthday",
    type: "text",
    required: true
  },
  {
    id: 3,
    name: "gender",
    type: "text",
    required: true,
  },
  {
    id: 4,
    name: "city",
    type: "text",
    required: true
  },
  {
    id: 4,
    name: "email",
    type: "email",
    required: true
  },
  {
    id: 5,
    name: "phone",
    type: "number",
    required: true
  },
  {
    id: 5,
    name: "site",
    type: "text",
    required: true
  },
  {
    id: 10,
    name: "terms",
    type: "boolean",
    required: true
  }
]

export const page = async () => {
  const init = async () => {
    try {
      console.log("Im Hereeeee")
      initPage()
      const request = await api.sendJustCode({phone, code: 9305})
      // if(request.userFound) location = '/'
      dm.termsGetValue()
      dm.click('submit', sendData)
      showForm()
      let phoneNumber = localStorage.getItem('up')
      // if(!phoneNumber) location = '/'
      doc.gId('phone').value = phoneNumber

    } catch (error) {
      console.log(error)
    }
  }
  
  init()
}

// Show register form
function showForm() {
  setTimeout( () => {
    win.scrollTo({
      top: doc.que('.refScroll').offsetTop,
      behavior: 'smooth'
    })
  }, 500 )

}

async function sendData(e) {
  e.preventDefault()
  const infoToSend = getData(inputsForm)
  const passed = renderErrors(infoToSend.resultValidation)
  console.log("passed", passed)
  if (passed) {
    console.log("info To send", infoToSend.infoToSend)
    sendDataApi(infoToSend.infoToSend)
  }
}

async function sendDataApi(data) {
  console.log('Im hereeee:', data)
  const request = await api.registerUser(data)
  dataDataLayers('register', data)
  if (request.status) {
    localStorage.setItem('ut', request.token)
    location = '/juego';
  } else if (request.code == 4) {
    localStorage.setItem('ut', request.token)
    location = '/juego';
  } else {
    dm.print('request', request.message);
  }
}

function getData(data) {
  let infoToSend = {}
  let resultValidation = []
  let currentCounty = ''
  const result = data.map(item => {
    const inputValue = doc.gId(`${item.name}`).value
    if (item.name == "country") currentCounty = inputValue
    const validationR = dm.globalValidation(item, inputValue, currentCounty)
    resultValidation = [...resultValidation, {
      name: item.name,
      valid: validationR.valid,
      message: validationR.message
    }]
    infoToSend = {
      ...infoToSend,
      [`${item.name}`]: inputValue
    }
  })
  const answer = {
    infoToSend: infoToSend,
    resultValidation: resultValidation
  }
  return answer
}

function initPage() {

  doc.addEv('DOMContentLoaded', e => {
    // BIRTHDAY
    const birthday = doc.que('#birthday')

    if (birthday) {
      birthday.addEv('keydown', resetDate)
    }

    // RESET DATE
    function resetDate(e) {

      const aloneNumbers = [8, 46, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57],
        elDate = e.target,
        dateLength = elDate.value.length

      // Validating the key code 
      if (aloneNumbers.indexOf(e.keyCode) === -1) {
        e.preventDefault()
      }

      // Adding the slash
      if (e.keyCode !== 8 && e.keyCode !== 46) {
        if (dateLength !== 1 || dateLength !== 3) {
          if (e.keyCode == 47) {
            e.preventDefault()
          }
        }

        if (dateLength === 2 || dateLength === 5) {
          e.target.value += '/'
        }
      }

    }


    // SCROLL SCROLL
    const scrollDown = doc.que('.scrollDown')

    if (scrollDown) {
      scrollDown.addEv('click', e => {
        doc.que('.refScroll').scrollIntoView({
          block: 'center',
          behavior: 'smooth'
        })
      })
    }

    // MODAL
    modal()

    // CHOICES
    const selects = doc.queAll('.form__select')

    if (selects.length > 0) {
      Array.from(selects).map(async selectItem => {
        // let temp
        // if(selectItem.id == "country"){
        //   const countries = await getAllCountries()
        //   setConuntriesSelect(countries)
        // }

        if (selectItem.id.toLowerCase() === 'city') {

          const cities = await getAllCities()
          setStatesSelect(cities)
          const selectEl = new Choices(selectItem, {
            searchResultLimit: 2,
            classNames: {
              containerOuter: 'choices city',
            }
          })

        } else {

          const selectEl = new Choices(selectItem, {
            searchResultLimit: 2
          })

        }

      })
    }


  })
}

// async function getAllCountries() {
//   try {
//     const result = await api.getAllCountries()
//     return result;
//   } catch (error) {
//     console.log("Error", error)
//   }
// }

// function setConuntriesSelect(countries) {
//   const dropdownCountries = document.getElementById("country");
//   const defaultOption = dropdownCountries.innerHTML
//   const listItems = countries.reduce((acc, current) => {
//     if(current){
//       acc += `<option value="${current['shortname']}">${current['name']}</option>`;
//       return acc;
//     }
//   }, '');
//   if (dropdownCountries) dropdownCountries.innerHTML = `${defaultOption}${listItems}`;
//   return dropdownCountries
// }

async function getAllStates() {
  try {
    const result = await api.getAllStates()
    return result
  } catch (error) {
    console.log("Error", error)
  }
}

// Get colombian cities from api
async function getAllCities() {
  try {
    const result = await api.getAllCities()
    return result
  } catch (error) {
    console.log('Error', error)
  }
}

function setStatesSelect(states) {
  const dropdownStates = document.getElementById("city");
  const defaultOption = dropdownStates.innerHTML
  const listItems = states.reduce((acc, current) => {
    if (current) {
      acc += `<option value="${current['name']}">${current['name']}</option>`;
      return acc;
    }
  }, '');
  if (dropdownStates) dropdownStates.innerHTML = `${defaultOption}${listItems}`;
  return dropdownStates
}

function renderErrors(validationList) {
  const result = validationList.every(item => {
    // console.log("Item Name", item.name, "Item valid", item.valid)
    return item.valid
  })
  if (!result) {
    validationList.map(data => {
      const inputEl = doc.gId(`${data.name}`),
        tag = doc.que(`[data-tag="${data.name}"]`)
      if (!data.valid) {
        if (tag) tag.innerHTML = data.message
        if( inputEl.nodeName.toLowerCase() === 'select' ){
          inputEl.closest('.choices').classList.add('error')
        } else if( inputEl.nodeName.toLowerCase() === 'input' ){
          inputEl.classList.add('error')
        }
      } else {
        if (tag) tag.innerHTML = ""
        if( inputEl.nodeName.toLowerCase() === 'select' ){
          inputEl.closest('.choices').classList.remove('error')
        } else if( inputEl.nodeName.toLowerCase() === 'input' ){
          inputEl.classList.remove('error')
        }
      }
    })
  }
  return result
}

// function clearForm(data){
//   console.log("Cleannn")
//   const result = data.map(item => {
//     console.log("item", item)
//     const input = document.getElementById(`${item.name}`)
//     input.value = ""
//   })

//   return result
// }




function dataDataLayers(select, data = []){

  let dataDataLayers = [
      {
          'name': 'next',
          'event': 'trackEvent',
          'eventCategory': 'Registro BBC', // Categoría del evento (String). Requerido.
          'eventAction': 'Teléfono', // Acción o subcategoría del evento (String). Requerido.
          'eventLabel': '' // Etiqueta de descripción del evento (String). Requerido.
      },
      {
          'name': 'register',
          'event': 'trackEvent',
          'eventCategory': 'Registro BBC', // Categoría del evento (String). Requerido.
          'eventAction': 'Registro', // Acción o subcategoría del evento (String). Requerido.
          'eventLabel': CryptoJS.SHA256(data['email']) // Etiqueta de descripción del evento (String). Requerido.
      }    
    ]
    const dataLayerFound =  dataDataLayers.find( value => value.name == select);
  // if(typeof dataDataLayers != 'undefined'){
  //   console.log('Im heereeeee')
  //   delete dataLayerFound['name']
  //   dataLayer.push(dataLayerFound);
  // }

}